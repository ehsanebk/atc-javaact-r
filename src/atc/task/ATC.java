package atc.task;

import java.awt.Color;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Vector;

import actr.task.Task;

public class ATC extends Task {
	private final double SAMPLE_TIME = .050;

	private static final Point[] SECTOR_POINTS = new Point[] { new Point(421, 88), new Point(432, 458),
			new Point(380, 458), new Point(380, 499), new Point(262, 528), new Point(165, 336) };
	private static final Point[][] SECTOR_LINES = new Point[][] {
			new Point[] { new Point(165, 336), new Point(119, 267) },
			new Point[] { new Point(237, 601), new Point(262, 528) },
			new Point[] { new Point(432, 456), new Point(432, 550) },
			new Point[] { new Point(432, 405), new Point(534, 515) },
			new Point[] { new Point(421, 88), new Point(485, 155) },
			new Point[] { new Point(421, 88), new Point(421, -2) } };

	private static final Object[][] CITY_SPECS = new Object[][] {
			new Object[] { "DES", new Point(154, 238), City.Type.LARGE },
			new Object[] { "NEB", new Point(80, 337), City.Type.LARGE },
			new Object[] { "IOW", new Point(256, 370), City.Type.LARGE },
			new Object[] { "KAN", new Point(215, 488), City.Type.LARGE },
			new Object[] { "IND", new Point(347, 370), City.Type.LARGE },
			new Object[] { "CIN", new Point(391, 370), City.Type.LARGE },
			new Object[] { "ONH", new Point(391, 292), City.Type.LARGE },
			new Object[] { "SPL", new Point(391, 238), City.Type.LARGE },
			new Object[] { "ONW", new Point(469, 258), City.Type.LARGE },
			new Object[] { "OHN", new Point(552, 230), City.Type.LARGE },
			new Object[] { "OHO", new Point(564, 373), City.Type.LARGE },
			new Object[] { "OHS", new Point(577, 525), City.Type.LARGE },
			new Object[] { "MINSP", new Point(377, 22), City.Type.MEDIUM },
			new Object[] { "BUTTE", new Point(267, 151), City.Type.MEDIUM },
			new Object[] { "SIOUX", new Point(41, 284), City.Type.MEDIUM },
			new Object[] { "PAWNE", new Point(-5, 368), City.Type.MEDIUM },
			new Object[] { "LINCO", new Point(102, 37), City.Type.MEDIUM },
			new Object[] { "INDIN", new Point(307, 370), City.Type.MEDIUM },
			new Object[] { "LOUIV", new Point(657, 484), City.Type.MEDIUM },
			new Object[] { "BOSTN", new Point(668, 230), City.Type.MEDIUM },
			new Object[] { "DETRO", new Point(552, 121), City.Type.MEDIUM },
			new Object[] { "COLUM", new Point(483, 370), City.Type.MEDIUM },
			new Object[] { "WHEEL", new Point(483, 460), City.Type.MEDIUM },
			new Object[] { "GEND2", new Point(397, 552), City.Type.MEDIUM },
			new Object[] { "DARIO", new Point(345, 442), City.Type.MEDIUM },
			new Object[] { "CHIGO", new Point(306, 432), City.Type.MEDIUM },
			new Object[] { "ILL", new Point(297, 695), City.Type.MEDIUM },
			new Object[] { "TOPKA", new Point(200, 540), City.Type.MEDIUM },
			new Object[] { "KAN2", new Point(249, 459), City.Type.SMALL },
			new Object[] { "TOPKA2", new Point(220, 536), City.Type.SMALL },
			new Object[] { "DAVES", new Point(298, 324), City.Type.SMALL },
			new Object[] { "KEN14", new Point(329, 449), City.Type.SMALL },
			new Object[] { "LANCE", new Point(303, 540), City.Type.SMALL },
			new Object[] { "GAARY", new Point(302, 573), City.Type.SMALL },
			new Object[] { "PEORA", new Point(299, 616), City.Type.SMALL },
			new Object[] { "TOLDO", new Point(352, 537), City.Type.SMALL },
			new Object[] { "BENNS", new Point(396, 474), City.Type.SMALL },
			new Object[] { "DAYSO", new Point(489, 296), City.Type.SMALL },
			new Object[] { "DAYSO2", new Point(444, 338), City.Type.SMALL },
			new Object[] { "WELLS", new Point(368, 101), City.Type.SMALL },
			new Object[] { "INDIN1", new Point(306, 397), City.Type.SMALL },
			new Object[] { "MIN", new Point(364, 54), City.Type.SMALL },
			new Object[] { "MIC", new Point(482, 40), City.Type.SMALL },
			new Object[] { "MAS", new Point(588, 66), City.Type.SMALL },
			new Object[] { "MAI1", new Point(605, 84), City.Type.SMALL },
			new Object[] { "MAI2", new Point(610, 94), City.Type.SMALL } };

	private static final String[][] ROUTE_SPECS = new String[][] {
			new String[] { "DES", "IOW", "CHIGO", "LANCE", "GAARY", "PEORA" },
			new String[] { "BUTTE", "INDIN", "INDIN1", "CHIGO", "LANCE", "GAARY", "PEORA" },
			new String[] { "WELLS", "SPL", "ONH", "CIN", "GEND2" },
			new String[] { "DAYSO", "DAYSO2", "CIN", "KAN2", "TOPKA2" },
			new String[] { "KAN", "KEN14", "DARIO", "OHO" }, new String[] { "KAN", "DAVES", "MIC" },
			new String[] { "KAN", "DAVES", "MAS" }, new String[] { "KAN", "DAVES", "MAI2" },
			new String[] { "DES", "DAVES", "SPL", "DETRO", "MAI1" },
			new String[] { "DES", "INDIN", "IND", "CIN", "COLUM" }, new String[] { "TOLDO", "DARIO", "DES" },
			new String[] { "TOLDO", "DARIO", "MIN" }, new String[] { "TOLDO", "DARIO", "SPL" },
			new String[] { "TOLDO", "DARIO", "DETRO" } };
	
	private static final String[][] ROUTE_SPECS_CONFLICT = new String[][] {
		new String[] {  "CHIGO","SPL"  },
		new String[] { "IOW","SPL" } };

	private Map<String, City> cities;
	private List<Route> routes;
	private List<Flight> flights;

	public ATC() {
		super();
		setBackground(Color.black);

		add(new Sector(SECTOR_POINTS));
		
//		for (int i = 0; i < SECTOR_LINES.length; i++)
//			add(new SectorLine(SECTOR_LINES[i]));

		cities = new HashMap<String, City>();
		for (int i = 0; i < CITY_SPECS.length; i++) {
			City city = new City(this, (String) CITY_SPECS[i][0], (Point) CITY_SPECS[i][1],
					(City.Type) CITY_SPECS[i][2]);
			cities.put(city.getName(), city);
			add(city);
		}

//		routes = new Vector<Route>();
//		for (int i = 0; i < ROUTE_SPECS.length; i++) {
//			String[] specs = ROUTE_SPECS[i];
//			Point[] points = new Point[specs.length];
//			for (int j = 0; j < specs.length; j++) {
//				City city = cities.get(specs[j]);
//				if (city != null)
//					points[j] = city.getPoint();
//				else
//					error(specs[j] + " not defined");
//			}
//			Route route = new Route(points);
//			routes.add(route);
//			add(route);
//		}
		
		routes = new Vector<Route>();
		for (int i = 0; i < ROUTE_SPECS_CONFLICT.length; i++) {
			String[] specs = ROUTE_SPECS_CONFLICT[i];
			Point[] points = new Point[specs.length];
			for (int j = 0; j < specs.length; j++) {
				City city = cities.get(specs[j]);
				if (city != null)
					points[j] = city.getPoint();
				else
					error(specs[j] + " not defined");
			}
			Route route = new Route(points);
			routes.add(route);
			add(route);
		}

		flights = new Vector<Flight>();
		Random random = new Random();
		for (Route route : routes) {
			String name = "AA" + (100 + random.nextInt(899));
			double traveled = random.nextInt((int) Math.floor(route.getLength()));
			Flight flight = new Flight(this, route, name);
			flight.moveTo(traveled);
			flights.add(flight);
			add(flight, 0);
		}

		repaint();
	}

	@Override
	public void start() {
		repaint();
		addUpdate(0);
		//addPeriodicUpdate(SAMPLE_TIME);
	}

	@Override
	public void update(double time) {
//		for (Flight plane : flights)
//			plane.moveBy(.1);
		repaint();
		processDisplay();
	}

	@Override
	public void typeKey(char c) {
	}

	public void error(String s) {
		System.out.println("ERROR: " + s);
	}
}
