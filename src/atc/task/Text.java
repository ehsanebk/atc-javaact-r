package atc.task;

import java.awt.Color;
import java.awt.Font;

import javax.swing.SwingConstants;

import actr.task.TaskLabel;

public class Text extends TaskLabel {

	public Text(String text, int x, int y, int width, int height) {
		super(text, x, y, width, height);
		setHorizontalAlignment(SwingConstants.LEFT);
		setVerticalAlignment(SwingConstants.TOP);
		setFont(new Font("Courier", Font.PLAIN, 9));
		setForeground(Color.green);
	}
}
