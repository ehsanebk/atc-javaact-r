package atc.task;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Stroke;
import java.awt.geom.GeneralPath;

import javax.swing.JPanel;

public class Route extends JPanel {
	private static final boolean USE_DASHED_LINE = false;

	private Point[] points;
	private GeneralPath polygon;
	private double length;

	public Route(Point[] points) {
		this.points = points;
		polygon = new GeneralPath();
		length = 0;
		if (points.length > 1) {
			polygon.moveTo(points[0].getX(), points[0].getY());
			for (int i = 1; i < points.length; i++) {
				Point next = points[i];
				length += polygon.getCurrentPoint().distance(next);
				polygon.lineTo(next.getX(), next.getY());
			}
		}
		setBounds(0, 0, (int) Math.round(polygon.getBounds().getMaxX()),
				(int) Math.round(polygon.getBounds().getMaxY()));
	}

	public double getLength() {
		return length;
	}

	public PointHeading getPointHeading(double distance) {
		if (points == null || points.length <= 1)
			return null;
		Point current = points[0];
		double distanceSoFar = 0;
		for (int i = 1; i < points.length; i++) {
			Point next = points[i];
			double distanceToNext = current.distance(next);
			if (distance <= distanceSoFar + distanceToNext) {
				double r = (distance - distanceSoFar) / distanceToNext;
				double x = (1 - r) * current.getX() + r * next.getX();
				double y = (1 - r) * current.getY() + r * next.getY();
				double heading = current.heading(next);
				return new PointHeading(x, y, heading);
			}
			current = next;
			distanceSoFar += distanceToNext;
		}
		Point point = points[points.length - 1];
		return new PointHeading(point.getX(), point.getY(), points[points.length - 2].heading(point));
	}

	@Override
	public void paintComponent(Graphics g) {
		Graphics2D g2 = (Graphics2D) g.create();
		if (USE_DASHED_LINE) {
			Stroke dashed = new BasicStroke(1, BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL, 0, new float[] { 1, 5 },
					0);
			g2.setStroke(dashed);
		}
		g2.setColor(Color.darkGray);
		g2.draw(polygon);
		g2.dispose();
	}
}
