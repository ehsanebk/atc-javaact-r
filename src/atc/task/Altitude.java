package atc.task;

import java.awt.Color;
import java.awt.Font;

import javax.swing.*;

import actr.task.TaskComponent;

/**
 * The class that defines a label for a task interface.
 * 
 * @author Dario Salvucci
 */
public class Altitude extends JLabel implements TaskComponent
{
	/**
	 * Creates a new label.
	 * @param text the label text
	 * @param x the x coordinate
	 * @param y the y coordinate
	 * @param width the width
	 * @param height the height
	 */
	private int altitude;
	public Altitude (String text, int x, int y, int width, int height) 
	{
		super (text);
		setBounds (x, y, width, height);
		setHorizontalAlignment(SwingConstants.LEFT);
		setVerticalAlignment(SwingConstants.TOP);
		setFont(new Font("Courier", Font.PLAIN, 9));
		setForeground(Color.green);
		
	}


	/**
	 * Gets the kind of component (i.e., the "kind" slot of the ACT-R visual object).
	 * @return the kind string
	 */
	public String getKind () { return "altitude"; }

	/**
	 * Gets the value of component (i.e., the "value" slot of the ACT-R visual object).
	 * @return the value string
	 */
	public String getValue () { return  getText() ;}
	
	
	
	
}

