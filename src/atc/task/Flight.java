package atc.task;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.Random;

import javax.swing.JPanel;

import actr.task.Task;
import actr.task.TaskComponent;

public class Flight extends JPanel implements TaskComponent {
	private static final int HEADING_LENGTH = 30;
	private static final int DATABLOCK_TEXT_DX = 75;
	private static final int DATABLOCK_TEXT_DY = 40;
	private static final int DATABLOCK_LINE_DX = 25;
	private static final int DATABLOCK_LINE_DY = 15;

	private Route route;
	private String callSign;
	private int assignedAltitude;
	private int currentAltitude;
	private double traveled;
	private PointHeading point;
	private Text dataBlock1;
	private Altitude dataBlock2;
	private Text dataBlock3;
	private double speed;

	public Flight(Task task, Route route, String callSign) {
		super();
		this.route = route;
		this.callSign = callSign;
		Random random = new Random();
		assignedAltitude = 330;
		currentAltitude = 100 + random.nextInt(599);
		traveled = 0;
		point = route.getPointHeading(traveled);
		setBounds(point.getIntX(), point.getIntY(), 1, 1);

		dataBlock1 = new Text(callSign, 0, 0, DATABLOCK_TEXT_DX, 10);
		dataBlock2 = new Altitude("", 0, 0, DATABLOCK_TEXT_DX, 10);
		dataBlock3 = new Text("", 0, 0, DATABLOCK_TEXT_DX, 10);
		task.add(dataBlock1, 0);
		task.add(dataBlock2, 0);
		task.add(dataBlock3, 0);
		fixDataBlock();
	}

	private void fixDataBlock() {
		int spacing = 10;
//		dataBlock2.setText(assignedAltitude + " " + (assignedAltitude == currentAltitude ? "-"
//				: assignedAltitude > currentAltitude ? "\u2191 " + currentAltitude : "\u2193 " + currentAltitude));
		dataBlock2.setText(""+currentAltitude);
		
		dataBlock3.setText("123 D 490");
		dataBlock1.setLocation(point.getIntX() - DATABLOCK_TEXT_DX, point.getIntY() - DATABLOCK_TEXT_DY);
		dataBlock2.setLocation(point.getIntX() - DATABLOCK_TEXT_DX, point.getIntY() - DATABLOCK_TEXT_DY + spacing);
		dataBlock3.setLocation(point.getIntX() - DATABLOCK_TEXT_DX, point.getIntY() - DATABLOCK_TEXT_DY + 2 * spacing);
	}

	public void moveTo(double traveled) {
		this.traveled = traveled;
		point = route.getPointHeading(traveled);
		setLocation(point.getIntX(), point.getIntY());
		fixDataBlock();
	}

	public void moveBy(double distance) {
		traveled += distance;
		if (traveled < 0)
			traveled += route.getLength();
		if (traveled > route.getLength())
			traveled -= route.getLength();
		moveTo(traveled);
	}

	public String getCallSign() {
		return callSign;
	}

	public int getAssignedAltitude() {
		return assignedAltitude;
	}

	public int getCurrentAltitude() {
		return assignedAltitude;
	}

	public void setAssignedAltitude(int altitude) {
		assignedAltitude = altitude;
		fixDataBlock();
	}

	public void setCurrentAltitude(int altitude) {
		currentAltitude = altitude;
		fixDataBlock();
	}

	public String getKind() {
		return "plane";
	}

	public String getValue() {
		return "plane";
	}

	@Override
	public void paintComponent(Graphics g) {
		int size = 3;
		int max = Math.max(Math.max(DATABLOCK_LINE_DX, DATABLOCK_LINE_DY), HEADING_LENGTH);
		Graphics2D g2 = (Graphics2D) g.create();
		g2.setClip(-max, -max, 2 * max, 2 * max);
		g2.setColor(Color.gray);
		g2.drawLine(-DATABLOCK_LINE_DX, -DATABLOCK_LINE_DY, -size - 2, -size - 1);
		g2.setColor(Color.green);
		Point start = new Point(0, 0).translateByHeading(HEADING_LENGTH / 3, point.getHeading() + Math.PI);
		Point end = new Point(0, 0).translateByHeading(HEADING_LENGTH, point.getHeading());
		g2.drawLine(start.getIntX(), start.getIntY(), end.getIntX(), end.getIntY());
		g2.drawRect(-size, -size, 2 * size, 2 * size);
		g2.dispose();
	}
}
