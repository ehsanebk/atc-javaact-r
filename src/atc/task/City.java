package atc.task;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.GeneralPath;

import javax.swing.JPanel;

import actr.task.Task;
import actr.task.TaskComponent;

public class City extends JPanel implements TaskComponent {
	private static final boolean ADD_LABEL = false;

	public enum Type {
		LARGE, MEDIUM, SMALL
	};

	private String name;
	private Point point;
	private Type type;
	private Text nameLabel;

	public City(Task task, String name, Point point, Type type) {
		this.name = name;
		this.point = point;
		this.type = type;
		setBounds(point.getIntX(), point.getIntY(), 20, 20);
		if (ADD_LABEL) {
			nameLabel = new Text(name, point.getIntX() - 25, point.getIntY() + 10, 50, 15);
			task.add(nameLabel);
		}
	}

	@Override
	public String getName() {
		return name;
	}

	public Point getPoint() {
		return point;
	}

	public Type getType() {
		return type;
	}

	public String getKind() {
		return "city";
	}

	public String getValue() {
		return "city";
	}

	private static GeneralPath triangle = null;

	@Override
	public void paintComponent(Graphics g) {
		int size = 3;
		Graphics2D g2 = (Graphics2D) g.create();
		g2.setClip(-size, -size, 2 * size + 1, 2 * size + 1);
		g2.setColor(new Color(.5f, .5f, .5f, .5f));
		if (triangle == null) {
			triangle = new GeneralPath();
			triangle.moveTo(0, -size + 1);
			triangle.lineTo(size, size);
			triangle.lineTo(-size, size);
			triangle.closePath();
		}
		g2.draw(triangle);
		g2.dispose();
	}
}
