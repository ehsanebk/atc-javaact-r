package atc.task;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.GeneralPath;

import javax.swing.JPanel;

public class Sector extends JPanel {
	private GeneralPath polygon;

	public Sector(Point[] points) {
		polygon = new GeneralPath();
		if (points.length > 1) {
			polygon.moveTo(points[0].getX(), points[0].getY());
			for (int i = 1; i < points.length; i++)
				polygon.lineTo(points[i].getX(), points[i].getY());
			polygon.closePath();
		}
		setBounds(0, 0, (int) Math.round(polygon.getBounds().getMaxX()),
				(int) Math.round(polygon.getBounds().getMaxY()));
	}

	@Override
	public void paintComponent(Graphics g) {
		Graphics2D g2 = (Graphics2D) g.create();
		g2.setColor(Color.lightGray);
		g2.draw(polygon);
		g2.dispose();
	}
}
